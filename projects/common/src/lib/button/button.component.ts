import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'msg-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  @Input()
  text = '';

  constructor() {}

  ngOnInit(): void {}
}
